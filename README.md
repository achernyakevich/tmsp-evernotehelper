## Intro ##

The `EvernoteHelper.user.js` designed to bring additional functionality and UI
enhancements to [Evernote](https://www.evernote.com/).


## Release Notes ##

Provide the following shortcuts:

* `Ctrl+Shift+Slash` - turn on/off Evernote search (if available on the screen).
* `Ctrl+Shift+Backslash` - tunr on/off full screen mode.


## Contribution guidelines ##

If you would like to contribute - create a pull request.

If you need some features or would like to propose some features - create an
issue.


## Release Notes ##

## v. 0.5.1 ##

### Misc ###

* Fixed typo in function name.


## v. 0.5 ##

### Misc ###

* Migrated to new public repository.
* Added metainformation for supporting automatic updates and public activities.
