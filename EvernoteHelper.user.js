// ==UserScript==
// @name         EvernoteHelper
// @namespace    https://bitbucket.org/achernyakevich/tmsp-evernotehelper/
// @version      0.5.1
// @description  This helper scripts bring some keyboard automation for web client of Evernote.
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        https://www.evernote.com/*
// @grant        GM_log
// @grant        GM_registerMenuCommand
// @noframes
// ==/UserScript==

(function() {
    'use strict';

    function toggleSearchPanel() {
        let searchButton = document.getElementById('qa-SEARCH_BAR_INITIAL');
        searchButton.click();
    }
    function toggleFullScreenMode() {
        let toggleButton = document.getElementById('qa-NOTE_FULLSCREEN_BTN');
        toggleButton.click();
    }

    setTimeout(() => {
        document.addEventListener('keydown', function(event) {
            //GM_log("Ctrl: " + event.ctrlKey +"; Shift: " + event.shiftKey + "; Key: " + event.key + "; Code: " + event.code);

            // Ctrl+Shift+? -> Toggle Search Panel (if available)
            // Ctrl+Shift+/ -> Toggle Full Screen mode
            if ( event.ctrlKey && event.shiftKey && ( event.code == 'Backslash' || event.code == 'Slash' ) ) {
                if ( event.code == 'Slash' ) {
                    toggleSearchPanel();
                } else if ( event.code == 'Backslash' ) {
                    toggleFullScreenMode();
                }
                event.stopPropagation();
                event.preventDefault();
            }

        }, true);
        GM_log("Shortcuts aassigned");
    }, 10000);

    GM_registerMenuCommand("Toggle left menu", toggleFullScreenMode, 't');
})();
